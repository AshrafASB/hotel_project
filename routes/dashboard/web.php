<?php

use Illuminate\Support\Facades\Route;

Route::prefix('dashboard')
    ->name('dashboard.')
//    ->middleware(['auth'])
    ->group(function (){

    //dashboard.welcome  - welcome_Route
        Route::get('/','welcomeController@index')->name('welcome');

    //category Routes
        Route::resource('categories','CategoryController')->except(['show']);

    //Role Route
        Route::resource('roles','RoleController');

    //User Route
        Route::resource('users','UserController');

   //floors Route
        Route::resource('floors','FloorController');

   //Rooms Route
        Route::resource('rooms','RoomController');

   //Playground Route
        Route::resource('playgrounds','PlaygroundController');

   //assign Route
        Route::resource('assign','AssignController');


    });
