<?php

use App\User_Playground;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'name'=>'Mohammad',
            'email'=>'super_admin@super.com',
            'password'=>bcrypt('123456'),
            'email_verified_at'=>Carbon::now(),
            'image'=>'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=pexels-pixabay-220453.jpg&fm=jpg'

        ]);
        \App\User::create([
            'name'=>'ahmad',
            'email'=>'ahmad@gmail.com',
            'password'=>bcrypt('123456'),
            'email_verified_at'=>Carbon::now(),
            'image'=>'https://images.pexels.com/photos/1043471/pexels-photo-1043471.jpeg?cs=srgb&dl=pexels-chloe-kala-1043471.jpg&fm=jpg'

        ]);

        \App\Floor::create([
            'name'=>'level 1',
            'level'=>'001',
            'numberOfRoom'=>'50',
        ]);

        \App\Floor::create([
            'name'=>'level 2',
            'level'=>'002',
            'numberOfRoom'=>'30',
        ]);

        \App\Room::create([
            'name'=>'Room A',
            'number'=>'001',
            'floor_id'=>1,
            'user_id'=>1,
        ]);

        \App\Playground::create([
            'name'=>'North Playground',
        ]);

        \App\Playground::create([
            'name'=>'South Playground',
        ]);


        \App\User_Playground::create([
            'user_id'=>1,
            'playground_id'=>1,
        ]);

        \App\User_Playground::create([
            'user_id'=>2,
            'playground_id'=>1,
        ]);

        \App\User_Playground::create([
            'user_id'=>1,
            'playground_id'=>2,
        ]);






        $user->attachRoles(['super_admin']);
    }
}
