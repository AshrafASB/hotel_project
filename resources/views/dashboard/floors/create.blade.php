







@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($floor)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update floors')}}
                    </i>
                </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                        {{__('site.Add floors') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            {{--            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>--}}
            <li class="breadcrumb-item"><a href="{{route('dashboard.floors.index')}}">{{__('site.Floors')}}</a></li>
            @if(isset($floor))
                <li class="breadcrumb-item"> {{ __('site.Update floors')}}</li>
            @else
                <li class="breadcrumb-item"> {{__('site.Add floors') }}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
                <form action="{{isset($floor)?route('dashboard.floors.update',$floor->id):route('dashboard.floors.store')}}" method="post">
                    @csrf
                    @if(isset($floor))
                        @method('put')
                    @else
                        @method('post')
                    @endif

                    @include('dashboard.partials._errors')

                    <div class="form-group">
                        <label>{{__('site.name')}} :</label>
                        <input type="text" name="name" class="form-control" value="{{isset($floor)?$floor->name:""}}">
                    </div>

                    <div class="form-group">
                        <label>{{__('site.level')}} :</label>
                        <input type="text" name="level" class="form-control" value="{{isset($floor)?$floor->level:""}}">
                    </div>


                    <div class="form-group">
                        <label>{{__('site.numberOfRoom')}} :</label>
                        <input type="number" name="numberOfRoom" class="form-control" value="{{isset($floor)?$floor->numberOfRoom:""}}">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            @if( isset($floor) )
                                <i class="fa fa-edit"></i>
                                {{__('site.Update')}}
                            @else
                                <i class="fa fa-plus"></i>
                                {{__('site.Add')}}
                            @endif

                        </button>
                    </div>
                </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
