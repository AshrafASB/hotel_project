@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($room)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update Rooms')}}
                    </i>
                </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                        {{__('site.Add Rooms') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            {{--            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>--}}
            <li class="breadcrumb-item"><a href="{{route('dashboard.rooms.index')}}">{{__('site.Rooms')}}</a></li>
            @if(isset($room))
                <li class="breadcrumb-item"> {{ __('site.Update Rooms')}}</li>
            @else
                <li class="breadcrumb-item"> {{__('site.Add Rooms') }}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
                <form action="{{isset($room)?route('dashboard.rooms.update',$room->id):route('dashboard.rooms.store')}}" method="post">
                    @csrf
                    @if(isset($room))
                        @method('put')
                    @else
                        @method('post')
                    @endif

                    @include('dashboard.partials._errors')

                    <div class="form-group">
                        <label>{{__('site.name')}} :</label>
                        <input type="text" name="name" class="form-control" value="{{isset($room)?$room->name:""}}">
                    </div>

                    <div class="form-group">
                        <label>{{__('site.Number')}} :</label>
                        <input type="number" name="number" class="form-control" value="{{isset($room)?$room->number:""}}">
                    </div>


                    {{-- Select of floors --}}
                    <div class="form-group">
                        <label>{{__('site.Floors follow')}} :</label>
                        <select name="floor_id" class="form-control">
                            @foreach( $floors as $floor )
                                <option value="{{$floor->id}}" {{isset($room)?($room->floor_id ==$floor->id?'selected':'' ):'' }}> {{ $floor->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    {{-- Select of floors --}}
                    <div class="form-group">
                        <label>{{__('site.User mind')}} :</label>
                        <select name="user_id" class="form-control">
                            @foreach( $users as $user )
                                <option value="{{$user->id}}" {{isset($room)?($room->user_id ==$user->id?'selected':'' ):'' }}> {{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            @if( isset($room) )
                                <i class="fa fa-edit"></i>
                                {{__('site.Update')}}
                            @else
                                <i class="fa fa-plus"></i>
                                {{__('site.Add')}}
                            @endif

                        </button>
                    </div>
                </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection









