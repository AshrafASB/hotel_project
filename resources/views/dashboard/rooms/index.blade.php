@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-list"></i> {{__('site.Rooms')}}</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
{{--            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>--}}
            <li class="breadcrumb-item">{{__('site.Rooms')}}</li>
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
                {{-- this form for Search button                --}}
                <form action="" >
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="search" autofocus class="form-control" placeholder="Search" value="{{request()->search}}">
                            </div>
                        </div>{{-- end-of-col-4 --}}


                        <div class="col-md-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>Search</button>
                                    <a href="{{route('dashboard.rooms.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                            </div>
                        </div>{{-- end-of-col-4 --}}


                    </div>{{-- end-of-row --}}
                </form>{{-- end-of-form --}}

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}

        <div class="row">
            <div class="col-md-12">
                <hr>
                @if($rooms->count() > 0 )
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('site.name | Room-number')}} </th>
                            <th>{{__('site.On the floor number')}}</th>
                            <th>{{__('site.User-specific')}}</th>
                            <th>{{__('site.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @isset($rooms)
                            @foreach($rooms as $index=>$room)
                                <tr>
                                    <td>{{++$index}}</td>
                                    <td>{{$room->name}} | {{$room->number}}</td>
                                    <td>{{$room->floor->name}}</td>
                                    <td>{{$room->user->name}}</td>
                                    <td>
                                        {{--Edit buttom--}}
                                        <a href="{{route('dashboard.rooms.edit', $room->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit">Edit</i></a>
                                        {{--Delete buttom--}}
                                        <form action="{{route('dashboard.rooms.destroy', $room->id)}}" method="post" style="display: inline-block">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i>{{__('site.Delete')}}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @endisset

                        </tbody>

                    </table>
                    {{$rooms->appends(request()->query())->links()}}
                @else
                    <h3 style="font-weight: 400; text-align: center"> No Record Found</h3>
                @endif
            </div>
        </div>
    </div>{{--end-of-tile mb-4--}}


@endsection
