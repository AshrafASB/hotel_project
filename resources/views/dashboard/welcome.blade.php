@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
    </div>

    <div class="container">
        <div class="brand">
            <h1 class="brand_name" style="text-align: center"><a href="{{route('home')}}">My Hotel</a></h1>
            <p class="brand_slogan" style="text-align: center">our Project hotel</p>
        </div><img src="{{asset('dashboard_files/images/logo.webp')}}" alt="" class="logo">
    </div>

@endsection
