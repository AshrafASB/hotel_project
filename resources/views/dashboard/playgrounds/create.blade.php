@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($playground)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update Playground')}}
                    </i>
                </h1>
            @elseif(isset($show_playground))
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Assign players')}}
                    </i>
                </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                        {{__('site.Add Playground') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            {{--            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>--}}
            <li class="breadcrumb-item"><a href="{{route('dashboard.playgrounds.index')}}">{{__('site.Playground')}}</a></li>
            @if(isset($playground))
                <li class="breadcrumb-item"> {{ __('site.Update Playground')}}</li>
            @elseif(isset($show_playground))
                <li class="breadcrumb-item"> {{ __('site.Assign players')}}</li>
            @else
                <li class="breadcrumb-item"> {{__('site.Add Playground') }}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ isset($playground)?route('dashboard.playgrounds.update',$playground->id) : isset($show_playground)?route('dashboard.assign.update',$show_playground->id): route('dashboard.playgrounds.store')  }}" method="post">
                    @csrf
                    @if(isset($playground))
                        @method('put')
                    @elseif(isset($show_playground))
                        @method('put')
                    @else
                        @method('post')
                    @endif

                    @include('dashboard.partials._errors')

                    <div class="form-group">
                        <label>{{__('site.name')}} :</label>
                        <input type="text" name="name" class="form-control" value="{{isset($playground)?$playground->name:"" || isset($show_playground)?$show_playground->name:"" }}">
                    </div>
                    @isset($show_playground)
                    <div class="form-group">
                        <label>{{__('site.users will play in')}} :</label>

                        <select name="users[]"  class="form-control select2" multiple>
                            @foreach( $users as $user )
                                @if( isset($show_playground) )
                                    <option
                                        value="{{$user->id}} "
                                        {{$show_playground->user?'selected':''}}
                                    >
                                        {{ $user->name}}
                                    </option>
                                @else
                                    <option value="{{$user->id}}">{{  $user->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    @endisset


                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            @if( isset($playground) )
                                <i class="fa fa-edit"></i>
                                {{__('site.Update')}}
                            @elseif(isset($show_playground))
                                <i class="fa fa-plus"></i>
                                {{__('site.Add')}}
                            @else
                                <i class="fa fa-plus"></i>
                                {{__('site.Add')}}
                            @endif

                        </button>
                    </div>
                </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection









