<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user"><img class="app-sidebar__user-avatar"
                    src="{{asset('dashboard_files/images/logo.jpg')}}"
                     style="width: 70px; height: 70px "
                    alt="User Image">
        <div>
            <p class="app-sidebar__user-name">{{auth()->user()->name}}</p>
{{--            <p class="app-sidebar__user-designation">{{implode(', ' , auth()->user()->roles->pluck('name')->toArray())}}</p>--}}
        </div>
    </div>
    <ul class="app-menu">

         <li>
            <li>
                 <a class="app-menu__item " href="{{route('dashboard.welcome')}}">
                     <i class="app-menu__icon fa fa-list"></i>
                     <span
                         class="app-menu__label">{{ __('site.Dashboard')}}
                     </span>
                 </a>
            </li>
            <li>
                 <a class="app-menu__item " href="{{route('dashboard.floors.index')}}">
                     <i class="app-menu__icon fas fa-gopuram"></i>
                     <span
                         class="app-menu__label">{{ __('site.Floors')}}
                     </span>
                 </a>
            </li>

            <li>
                <a class="app-menu__item " href="{{route('dashboard.users.index')}}">
                    <i class="app-menu__icon fa fa-users"></i>
                    <span
                        class="app-menu__label">{{ __('site.Users')}}
                    </span>
                </a>
            </li>

            <li>
                <a class="app-menu__item " href="{{route('dashboard.rooms.index')}}">
                    <i class="app-menu__icon fa fa-bed"></i>
                    <span
                        class="app-menu__label">{{ __('site.Rooms')}}
                </span>
                </a>
            </li>

            <li>
                <a class="app-menu__item " href="{{route('dashboard.playgrounds.index')}}">
                    <i class="app-menu__icon fas fa-futbol"></i>
                    <span
                        class="app-menu__label">{{ __('site.Playground')}}
                </span>
                </a>
            </li>




{{--        <li class="treeview">--}}
{{--            <a class="app-menu__item" href="#" data-toggle="treeview">--}}
{{--                <i class="app-menu__icon fa fa-laptop"></i>--}}
{{--                    <span class="app-menu__label">UI Elements</span>--}}
{{--                <i class="treeview-indicator fa fa-angle-right"></i>--}}
{{--            </a>--}}
{{--            <ul class="treeview-menu">--}}
{{--                <li>--}}
{{--                    <a class="treeview-item" href="bootstrap-components.html"><i class="icon fa fa-circle-o"></i>--}}
{{--                        Bootstrap Elements--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a class="treeview-item" href="https://fontawesome.com/v4.7.0/icons/" target="_blank"--}}
{{--                       rel="noopener"><i class="icon fa fa-circle-o"></i> Font Icons--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </li>--}}

    </ul>
</aside>
