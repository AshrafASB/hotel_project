<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Playground;
use App\User;
use App\User_Playground;
use Illuminate\Http\Request;

class PlaygroundController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.playgrounds.";
    }

    public function index()
    {

        $playgrounds = Playground::WhenSearch(request()->search)
            ->withCount('users')
            ->paginate(5);
        return view($this->path.'index',compact('playgrounds'));
    }//end of index

    public function create()
    {
        $users = User::all();
        return view($this->path.'create',compact(['floors','users']));
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:playgrounds,name',
        ]);

        Playground::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show (Playground $playground){
        $show_playground = $playground;
        $users = User::all();
        return view($this->path.'create',compact(['show_playground','users']));


    }//end of show

    public function edit(Playground $playground)
    {
        $users = User::all();
        return view($this->path.'create',compact(['playground','users']));
    }//end of edit

    public function update(Request $request, Playground $playground )
    {
            $request->validate([
                'name' => 'required|unique:playgrounds,name,'.$playground->id,
            ]);
            $playground->update($request->all());


        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Playground $playground)
    {
        $playground->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
