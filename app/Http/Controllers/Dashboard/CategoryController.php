<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
     public function __construct()
         {
             //Parent Path
             $this->path = "dashboard.categories.";

         }

          public function index()
          {
              $categories = Category::WhenSearch(request()->search)->paginate(5);
              return view($this->path.'index',compact('categories'));
          }//end of index

          public function create()
          {
               return view($this->path.'create');
          }//end of create

          public function store(Request $request)
          {
              $request->validate([
                  'name' => 'required|unique:categories,name',
              ]);
              Category::create($request->all());
              session()->flash('success',__('site.DataAddSuccessfully'));
              return redirect()->route($this->path.'index');
          }//end of store

          public function show($id)
          {
              //
          }//end of show

          public function edit(Category $category)
          {
               return view($this->path.'create',compact('category'));
          }//end of edit

          public function update(Request $request, Category $category)
          {
              $request->validate([
                  'name' => 'required|unique:categories,name,'.$category->id,
              ]);
              $category->update($request->all());
              session()->flash('success',__('site.DataUpdatedSuccessfully'));
              return redirect()->route($this->path.'index');
          }//end of update

          public function destroy(Category $category)
          {
              $category->delete();
              session()->flash('success',__('site.DataDeletedSuccessfully'));
              return redirect()->route($this->path.'index');
          }//end of destroy
}
