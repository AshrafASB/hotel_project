<?php

namespace App\Http\Controllers\Dashboard;
use App\Floor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class FloorController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.floors.";

    }

    public function index()
    {
        $floors = Floor::WhenSearch(request()->search)->paginate(5);
        return view($this->path.'index',compact('floors'));
    }//end of index

    public function create()
    {
        return view($this->path.'create');
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:floors,name',
            'level' => 'required',
            'numberOfRoom' => 'required|integer',
        ]);

        Floor::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store


    public function edit(Floor $floor)
    {
        return view($this->path.'create',compact('floor'));
    }//end of edit

    public function update(Request $request, Floor $floor)
    {
        $request->validate([
            'name' => 'required|unique:floors,name,'.$floor->id,
            'level' => 'required',
            'numberOfRoom' => 'required|integer',
        ]);
        $floor->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Floor $floor)
    {
        $floor->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
