<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;

use App\Playground;
use App\User_Playground;
use Illuminate\Http\Request;

class AssignController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.playgrounds.";
    }
    public function update  (Request $request, Playground $assign){

        foreach ($request->users as $user){
            User_Playground::create([
                'playground_id'=>$assign->id,
                'user_id'=>$user
            ]);
        }
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');

    }//end of update
}
