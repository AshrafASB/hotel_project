<?php

namespace App\Http\Controllers\Dashboard;
use App\Floor;
use App\Http\Controllers\Controller;
use App\Room;
use App\User;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.rooms.";
    }

    public function index()
    {
        $rooms = Room::WhenSearch(request()->search)
            ->with(['floor','user'])
            ->paginate(5);
        return view($this->path.'index',compact('rooms'));
    }//end of index

    public function create()
    {
        $floors = Floor::all();
        $users = User::all();
        return view($this->path.'create',compact(['floors','users']));
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|',
            'number' => 'required|',
            'user_id' => 'required|unique:rooms,user_id',
        ]);


        Room::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store


    public function edit(Room $room)
    {
        $floors = Floor::all();
        $users = User::all();
        return view($this->path.'create',compact(['room','floors','users']));
    }//end of edit

    public function update(Request $request, Room $room)
    {
        $request->validate([
            'name' => 'required|unique:rooms,name,'.$room->id,
            'number' => 'required|',
        ]);
        $room->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(Room $room)
    {
        $room->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
