<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Playground extends Model
{
    protected $table = 'users_playground';
    protected $guarded = [];
}
